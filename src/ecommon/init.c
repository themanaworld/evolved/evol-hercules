// Copyright (c) Copyright (c) Hercules Dev Team, licensed under GNU GPL.
// Copyright (c) 2014 - 2015 Evol developers

#include "common/hercules.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common/HPMi.h"

HPExport struct HPMi_interface HPMi_s;
HPExport struct HPMi_interface *HPMi;
HPExport void *(*import_symbol) (char *name, unsigned int pID);

#include "common/conf.h"
#include "common/memmgr.h"
#include "common/mapindex.h"
#include "common/mmo.h"
#include "common/socket.h"
#include "common/strlib.h"
#include "common/sysinfo.h"
#include "common/timer.h"

void interfaces_init_common(void)
{
}

void setDefaultMap(void)
{
    mapindex->default_map = aStrdup("000-1");
    mapindex->default_x = 22;
    mapindex->default_y = 22;
}

void commonClean(void)
{
    aFree(mapindex->default_map);
    mapindex->default_map = NULL;
}

#define checkVar(name, value) \
    if (name != value) \
    { \
        ShowError(#name" wrong value. Found %d but should be %d.\n", \
            name, \
            value); \
    }

void common_online(void)
{
    checkVar(MAX_SKILL_DB, 1585);
    checkVar(MAX_SKILL_ID, 20075);
    checkVar(SC_MAX, 1010);
    checkVar(SI_MAX, 1550);
    checkVar(OLD_MAX_SKILL_DB, 1510);
    checkVar(MAX_EVOL_SKILLS, 75);
    checkVar(EVOL_FIRST_SKILL, 20000);
    checkVar(MAX_SKILL_TREE, 125);
    checkVar(MAX_PARTY, 15);
    checkVar(BASE_GUILD_SIZE, 255);
}
