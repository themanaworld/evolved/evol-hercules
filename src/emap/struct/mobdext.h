// Copyright (c) Copyright (c) Hercules Dev Team, licensed under GNU GPL.
// Copyright (c) 2014 - 2015 Evol developers

#ifndef EVOL_MAP_MOBDEXT
#define EVOL_MAP_MOBDEXT

#include "map/map.h"

struct MobdExt
{
    int walkMask;
    int weaponAttacks[MAX_WEAPON_TYPE];
    unsigned short eleResist[ELE_MAX];
    int collisionDx;
    int collisionDy;
    int collisionMask;
    int criticalDef;
    unsigned short mutationCount;
    unsigned short mutationStrength;
};

#endif  // EVOL_MAP_MOBDEXT
